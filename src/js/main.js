"use strict";
var dBody = document.querySelector(".app");
var burgerBtn = document.querySelector(".burger-btn");
var navBar = document.querySelector(".navbar");
var activeBtn = document.querySelector(".burger-btn");

window.onload = function () {
  activeBtn.classList.remove("acttive-btn");
};

burgerBtn.addEventListener("click", function () {
  navBar.classList.toggle("hidden");
});
activeBtn.addEventListener("click", function () {
  activeBtn.classList.toggle("acttive-btn");
});

navBar.addEventListener("click", function (event) {
  var target = event.target;
  if (!target.classList.contains("navbar__link")) {
    navBar.classList.add("hidden");
    activeBtn.classList.remove("acttive-btn");
  }
});
